<?php

class XMLHttpService extends XMLHTTPRequestService
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * XMLHttpService constructor.
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * @param string $url
     * @param string $type
     * @param array $params
     * @return mixed
     */
    public function request(string $url, string $type, array $params = [])
    {
        $request = new \Request($type, $url, $params);

        return $request->getBody();
    }
}

class Http
{
    private $service;

    public function __construct(XMLHttpService $xmlHttpService)
    {
        $this->service = new XMLHttpService();
    }

    /**
     * @param string $url
     * @param array $options
     */
    public function get(string $url, array $options = [])
    {
        $this->service->request($url, 'GET', $options);
    }

    /**
     * @param string $url
     */
    public function post(string $url, array $options = [])
    {
        $this->service->request($url, 'POST', $options);
    }
}
