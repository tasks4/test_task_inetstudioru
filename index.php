<?php

$lists =
    [
        ['id' => 1, 'date' => "12.01.2020", 'name' => "test1"],
        ['id' => 2, 'date' => "02.05.2020", 'name' => "test2"],
        ['id' => 2, 'date' => "11.11.2020", 'name' => "test4"],
        ['id' => 4, 'date' => "08.03.2020", 'name' => "test4"],
        ['id' => 4, 'date' => "08.03.2020", 'name' => "test4"],
        ['id' => 1, 'date' => "22.01.2020", 'name' => "test1"],
        ['id' => 1, 'date' => "22.01.2020", 'name' => "test1"],
        ['id' => 3, 'date' => "06.06.2020", 'name' => "test3"],
        ['id' => 3, 'date' => "06.06.2020", 'name' => "test3"]
    ];


//1

function unique_multidim_array($array, $key)
{
    $temp_array = array();
    $i = 0;
    $key_array = array();

    foreach ($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}


$task1 = unique_multidim_array($lists, 'id');


//2

function sortArray(array $list, string $divider): array
{
    uasort(
        $list,
        function ($comparable, $comparison) use ($divider) {
            if (
                !isset($comparable[$divider])
                || $comparable[$divider] === null
                || $comparison[$divider] === null
                || $comparable[$divider] == $comparison[$divider]
            ) {
                return 0;
            }

            return ($comparable[$divider] < $comparison[$divider]) ? -1 : 1;
        }
    );

    return $list;
}

$task2 = sortArray($lists, 'id');


//3

$task3 = array_column($lists, 'id');


// 4

function task4(array $lists): array
{
    $newLists = [];

    foreach ($lists as $index => $list) {
        $newLists[$list['name']] = $list['id'];
    }

    return $newLists;
}

$task4 = task4($lists);

echo "Task 1\n";
var_dump($task1);

echo "Task 2\n";
var_dump($task2);

echo "Task 3\n";
var_dump($task3);

echo "Task 4\n";
var_dump($task4);
